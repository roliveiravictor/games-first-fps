# README #

First FPS game ever developed.

Title: First FPS

Engine: Unreal 4.9

Programming Language: C++

IDE: Visual Studio 2013

Note:

* **11/10/2015** - Implemented blueprint elevator mechanic with sound effects: https://www.youtube.com/watch?v=J7FR_VEtgc4. I will be probably trying to learn some text animation with flash in a few days to replace the simple "Survive" at game start or how to spawn enemies.

* **02/10/2015** - I'm learning some blueprint basics to move forward on this development. Things are going pretty well and I hope to make in a few days a new video to update what I've done since.

* **25/09/2015** - Code update from Unreal Engine 4.7 to 4.9. It has been a long time between this and the last update note, things got really creepy at my bachelor's ending haha I hope to get a great work sequence in this project from now on.

* **11/12/2014** - I implemented today my own HUD. It's pretty simples until now with just a crosshair aim. I also tried to implement a folder structure to make a better organization of the codes as I did in other repositories. The thing is, I couldn't do it because some restrictions by unreal are somehow blocking it. I believe it regards to Unreal references and its purpose is not mess around with the path of the codes. Anyway, I will do this structure when I'm done with the project.

* **12/11/2014** - Level design video released: https://www.youtube.com/watch?v=2sWxYkZPGZM. This is how the level design looks like so far. I didn't want to waste too much time with it, so I've imported a level from UE Marketplace. I made some customizations with some other imported assets, thus the map will fulfill my purposes. Now I can focus on programming the game behavior.

* **11/11/2014** - Today I've updated my UE4 version from 4.1 to 4.4; The reason was a bug with my custom game mode which was crashing the game even before it was running.

* **01/11/2014** - This project was first created to follow Alan Thorn's Book practical exercises. Unfortunately, Alan's book is outdated and I've needed to improvise with my own knowledge to go further with this development. Until now, only the above tutorial was used to guide me. By the time I need more information about how to develop X or Y functionality at unreal I will be adding those tutorials here. Also, it's good to mention that to increase my development difficulty, I'v e started challenge from a completely blank project.

### What is this repository for? ###

* This is a backup code of my first FPS game 3D ever developed
* Version 1.0  [Deprecated for now - 20.12.2015]

### How do I get set up? ###

* N/A

### Contribution guidelines ###

* First Person Shooter C++ Tutorial - https://wiki.unrealengine.com/First_Person_Shooter_C%2B%2B_Tutorial

### Who do I talk to? ###

* roliveira.victor@gmail.com


### Youtube ###

* Channel: https://www.youtube.com/channel/UC_rQ1n5XXcx6nqkXCAr2vTg/